export default {
  cardHeight: 134,
  cardWidth: 75,

  tableColumns: 19, // *must* be odd

  handSize: 5,

  roundLimit: 6,
};
