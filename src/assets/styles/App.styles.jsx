import React from "react";
import styled from "styled-components";

import Particles from "react-tsparticles";

const AppContainer = styled.div`
  background-color: black;
  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
`;

const ParticleBackground = (
  <Particles
    width={"100vw"}
    height={"100vh"}
    params={{
      particles: {
        number: {
          value: 60,
          density: {
            enable: true,
            value_area: 1500,
          },
        },
        line_linked: {
          enable: true,
          opacity: 0.02,
        },
        move: {
          direction: "right",
          speed: 0.1,
        },
        size: {
          value: 1,
        },
        opacity: {
          anim: {
            enable: true,
            speed: 1,
            opacity_min: 0.05,
          },
        },
      },
      interactivity: {
        events: {
          onclick: {
            enable: true,
            mode: "push",
          },
        },
        modes: {
          push: {
            particles_nb: 1,
          },
        },
      },
      retina_detect: true,
    }}
  />
);

export { AppContainer, ParticleBackground };
