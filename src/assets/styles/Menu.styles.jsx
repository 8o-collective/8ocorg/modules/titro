import styled from "styled-components";

const MenuContainer = styled.div`
  margin: 0px;
  background-color: black;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
`;

const MenuParticleContainer = styled.div`
  overflow-x: hidden;
  overflow-y: hidden;
  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
`;

const MenuTitle = styled.div`
  color: white;
  position: absolute;
  left: 50%;
  top: 30%;
  transform: translate(-50%, 0);
  font-family: "Times Pixelated 32 Bold Italic";
  font-size: 3vw;
`;

const MenuInformation = styled.div`
  color: white;
  position: absolute;
  left: 50%;
  top: 30%;
  transform: translate(-50%, 0);
  font-family: "Times Pixelated 24 Italic";
  font-size: 2vw;
`;

const MenuLink = styled.a`
  position: absolute;
  left: 50%;
  top: 50vh;
  transform: translate(-50%, -50%);

  font-family: "Courier Pixelated 16";
  color: red;
`;

const MenuButtonContainer = styled.div`
  position: absolute;
  left: 50%;
  top: 70vh;
  height: 20vh;
  width: 20vw;
  transform: translate(-50%, -50%);

  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const MenuButton = styled.div`
  width: 100%;

  border: 1px solid red;
  background-color: black;
  color: white;

  text-align: center;
  font-family: "Courier Pixelated 16";

  cursor: pointer;
`;

const MenuTextContainer = styled.div`
  position: absolute;
  left: 50%;
  top: 10vh;
  height: 20vh;
  width: 40vw;
  transform: translate(-50%, -50%);

  font-family: "Times Pixelated 16";

  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const MenuText = styled.div`
  width: 100%;
  left: 50%;
  color: #ddd;
  text-align: center;
  font-size: 20px;
  padding-top: 25px;
  padding-bottom: 25px;
`;

const MenuImage = styled.img`
  position: relative;
  width: 50%;
  left: 50%;
  transform: translateX(-50%);
  color: #ddd;
  font-size: 20px;
  padding-top: 25px;
  padding-bottom: 25px;

  cursor: pointer;
`;

export {
  MenuContainer,
  MenuParticleContainer,
  MenuTitle,
  MenuInformation,
  MenuLink,
  MenuButtonContainer,
  MenuButton,
  MenuTextContainer,
  MenuText,
  MenuImage,
};
