import styled from "styled-components";

const EndContainer = styled.div`
  margin: 0px;
  background-color: black;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
`;

const EndParticleContainer = styled.div`
  overflow-x: hidden;
  overflow-y: hidden;
  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
`;

const EndText = styled.div`
  color: white;
  position: absolute;
  left: 50%;
  top: 30%;
  transform: translate(-50%, 0);
  font-family: "Times Pixelated 24 Italic";
  font-size: 2vw;
`;

export { EndContainer, EndParticleContainer, EndText };
