import styled, { keyframes } from "styled-components";

const TurnArrowFlicker = keyframes`
  0%   { opacity:0; }
  30%  { opacity:1; }
  100% { opacity:0; }
`;

const TurnArrowImage = styled.img`
  display: block;
  position: absolute;
  height: 10%;
  bottom: 1%;
  right: 1%;
  animation: ${TurnArrowFlicker} 3s infinite linear;
  cursor: pointer;
`;

const TurnArrowTextOverlay = styled.div`
  position: fixed;
  font-family: monospace;
  top: 10px;
  left: 10px;
  font-size: 1em;
  text-align: center;
  color: white;

  user-select: none; /* supported by Chrome and Opera */
  -webkit-user-select: none; /* Safari */
  -khtml-user-select: none; /* Konqueror HTML */
  -moz-user-select: none; /* Firefox */
  -ms-user-select: none; /* Internet Explorer/Edge */

  visibility: hidden;
  opacity: 0;
  transition: opacity 0.5s, visibility 0.5s;

  ${TurnArrowImage}:hover + & {
    visibility: visible;
    opacity: 1;
    transition: opacity 0.5s, visibility 0.5s;
  }
`;

export { TurnArrowImage, TurnArrowTextOverlay };
