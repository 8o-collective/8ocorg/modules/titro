import styled from "styled-components";

const PlayerScore = styled.div`
  display: block;
  position: absolute;
  bottom: 1%;
  left: 1%;
  font-size: 20px;
  font-family: "Courier Pixelated 16 Bold";
  color: white;
`;

const OpponentScore = styled.div`
  display: block;
  position: absolute;
  top: 1%;
  right: 1%;
  font-size: 20px;
  font-family: "Courier Pixelated 16 Bold";
  color: white;
`;

export { PlayerScore, OpponentScore };
