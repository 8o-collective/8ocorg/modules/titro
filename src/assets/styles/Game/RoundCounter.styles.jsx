import styled from "styled-components";

const RoundCounter = styled.div`
  display: block;
  position: absolute;
  bottom: 1%;
  right: 1%;
  font-size: 20px;
  font-family: "Courier Pixelated 16 Bold";
  color: white;
`;

export { RoundCounter };
