import styled from "styled-components";

const NotificationContainer = styled.div`
  position: absolute;
  width: 100%;
  height: 10vh;
  line-height: 10vh;
  top: 50%;
  left: 0%;
  transform: translate(0, -50%);

  background-color: rgba(0, 0, 0, 0.7);
  border-color: white;
  border-style: solid;
  border-width: 0.25vh 0px;

  text-align: center;
  color: white;
  font-size: 5vh;
  font-family: "Times Pixelated 32 Italic";
`;

export { NotificationContainer };
