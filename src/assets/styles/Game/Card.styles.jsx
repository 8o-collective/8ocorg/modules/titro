import styled, { keyframes, css } from "styled-components";

import constants from "assets/constants.js";

const CardFlicker = keyframes`
  0%   { opacity:0; }
  50%  { opacity:1; }
  100% { opacity:0; }
`;

const fadeOut = keyframes`
  0%   { opacity:1; }
  1%   { opacity:1; }
  100% { opacity:0; }
`;

const CardContainer = styled.div`
  background: url(${(props) => props.img});
  background-size: 100% 100%;
  display: flex;

  ${({ guide }) =>
    guide &&
    css`
      pointer-events: none;
      animation: ${CardFlicker} 2s infinite linear;
    `}

  &::before {
    content: "";
    display: block;
    height: 0;
    width: 0;
    padding-bottom: calc(${constants.cardHeight / constants.cardWidth} * 100%);
  }
`;

// we use the... "unique" :before modifier to force the aspect ratio on all of the grid elements

const CardVisibleOverlay = styled.img`
  width: 100%;
  height: 100%;
  margin: 0px;
  padding: 0px;

  ${({ cloak }) =>
    cloak &&
    css`
      -webkit-mask-image: -webkit-gradient(
        linear,
        left top,
        left bottom,
        from(rgba(0, 0, 0, 1)),
        to(rgba(0, 0, 0, 0))
      );
      mask-image: linear-gradient(
        to bottom,
        rgba(0, 0, 0, 1),
        rgba(0, 0, 0, 0)
      );
    `}

  user-drag: none;
  user-select: none;

  visibility: visible;
  opacity: 1;
  transition: opacity 0.5s, visibility 0.5s;

  // ${({ fade }) =>
    fade &&
    css`
      //   animation: ${fadeOut} 1s linear;
      //
    `}

  ${CardContainer}:hover & {
    visibility: hidden;
    opacity: 0;
    transition: opacity 0.5s, visibility 0.5s;
  }
`;

const CardTextOverlay = styled.div`
  position: fixed;
  font-family: "Courier Pixelated 16";
  top: 10px;
  left: 10px;
  font-size: 1em;
  text-align: center;
  color: white;

  user-select: none;

  visibility: hidden;
  opacity: 0;
  transition: opacity 0.5s, visibility 0.5s;

  ${CardContainer}:hover + & {
    visibility: visible;
    opacity: 1;
    transition: opacity 0.5s, visibility 0.5s;
  }
`;

export { CardContainer, CardVisibleOverlay, CardTextOverlay };
