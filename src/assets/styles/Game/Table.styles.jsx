import styled from "styled-components";

import constants from "../../constants.js";

const TableContainer = styled.div`
  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  display: grid;
  grid-template-rows: [opponent-hand] 1fr [opponent-attack] 1fr [mid] 1fr [player-attack] 1fr [player-hand] 1fr [bottom];
  grid-template-columns: repeat(${constants.tableColumns}, 1fr);
  grid-row-gap: calc(
    (
        100vh - 5 *
          (
            ${100 / constants.tableColumns}vw *
              ${constants.cardHeight / constants.cardWidth}
          )
      ) / 4
  );
`;

const TableParticleContainer = styled.div`
  overflow-x: hidden;
  overflow-y: hidden;
  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  z-index: -1;
`;

export { TableContainer, TableParticleContainer };
