const getNameFromValue = (obj, value) => {
  const keys = Object.keys(obj);
  if (keys === []) throw "Cannot find key in non-enum object";

  const key = keys.find((key) => obj[key] === value);
  if (key === undefined)
    throw `Cannot find key with value ${value} in object ${obj}`;

  return key.toLowerCase();
};

export { getNameFromValue };
