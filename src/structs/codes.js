const Code = Object.freeze({
  TOKEN: 0, // token to use in requests to server

  CONNECT: 1, // another player has connected
  DISCONNECT: 2, // other player disconnected

  SCORE: 3, // update score
  ROUND: 4, // round change
  PHASE: 5, // phase change

  HAND: 6, // array of player hand
  SPREAD: 7, // array of table spread

  ATTACK: 8, // position and value of attacking card for round from client

  END: 9, // game end signal

  MALFORMED: -1, // input is malformed or cannot be parsed
  IMPOSSIBLE: -2, // move chosen is desynchronized from server game state
});

const ClientCode = Object.freeze({
  GRABBING: 10, // card has been grabbed
  ATTACKING: 11, // user has placed attack
  TURN: 12, // user has submitted attack
  READY: 13, // user is ready to move onto next round
});

const Result = Object.freeze({
  TIE: -1,
  LOSS: 0,
  WIN: 1,
  DISCONNECT: 10,
});

export { Code, ClientCode, Result };
