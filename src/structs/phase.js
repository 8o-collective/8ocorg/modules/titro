const Phase = Object.freeze({
  PREP: 0,
  PRIMA: 1,
  RESPAIDA: 2,
  CLEANUP: 3,
});

export { Phase };
