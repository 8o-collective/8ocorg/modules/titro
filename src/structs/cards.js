const Suit = Object.freeze({
  MAJORARCANA: 0,
  CUPS: 1,
  PENTACLES: 2,
  WANDS: 3,
  SWORDS: 4,
});

const Rank = Object.freeze({
  ACE: 1,
  TWO: 2,
  THREE: 3,
  FOUR: 4,
  FIVE: 5,
  SIX: 6,
  SEVEN: 7,
  EIGHT: 8,
  NINE: 9,
  TEN: 10,
  PAGE: 11,
  KNIGHT: 12,
  QUEEN: 13,
  KING: 14,
});

const MajorArcana = Object.freeze({
  FOOL: 0,
  MAGICIAN: 1,
  HIGHPRIESTESS: 2,
  EMPRESS: 3,
  EMPEROR: 4,
  HIEROPHANT: 5,
  LOVERS: 6,
  CHARIOT: 7,
  STRENGTH: 8,
  HERMIT: 9,
  WHEELOFFORTUNE: 10,
  JUSTICE: 11,
  HANGEDMAN: 12,
  DEATH: 13,
  TEMPERANCE: 14,
  DEVIL: 15,
  TOWER: 16,
  STAR: 17,
  MOON: 18,
  SUN: 19,
  JUDGEMENT: 20,
  WORLD: 21,
});

export { Suit, Rank, MajorArcana };
