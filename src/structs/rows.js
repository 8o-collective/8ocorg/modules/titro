const Rows = Object.freeze({
  // i would have these be (-2, 2) with mid as 0 but CSS rows start at 1 which makes it hard
  Opponent: Object.freeze({
    HAND: 1,
    ATTACK: 2,
  }),
  MID: 3,
  Player: Object.freeze({
    ATTACK: 4,
    HAND: 5,
  }),
});

const amountRows = 5;

export { Rows, amountRows };
