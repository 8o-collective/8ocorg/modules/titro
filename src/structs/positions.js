import constants from "assets/constants.js";

const Positions = Object.freeze({
  BLIND: -1,
  CENTER: 0,
  CLOAK: 1,
});

const positionToColumn = (position) =>
  [midColumn - 3, midColumn, midColumn + 3][position + 1];
const columnToPosition = (column) => {
  let index = [midColumn - 3, midColumn, midColumn + 3].indexOf(column);
  return index !== -1 ? index - 1 : null; // subtract one if found, null if not found
};

const midColumn = Math.ceil(constants.tableColumns / 2);

export { Positions, positionToColumn, columnToPosition, midColumn };
