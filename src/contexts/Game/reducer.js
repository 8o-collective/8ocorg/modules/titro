import { ClientCode, Code, Result } from "structs/codes.js";
import { Positions } from "structs/positions.js";

export const reducer = (state, action) => {
  console.log(action);

  switch (action.code) {
    case Code.TOKEN:
      return {
        ...state,
        token: action.data,
      };

    case Code.CONNECT:
      return {
        ...state,
        connect: true,
      };
    case Code.DISCONNECT:
      return {
        ...state,
        end: {
          result: Result.DISCONNECT,
        },
      };

    case Code.SCORE:
      return {
        ...state,
        scores: action.data,
      };
    case Code.ROUND:
      return {
        ...state,
        ...initialRoundState,
        round: action.data,
      };
    case Code.PHASE:
      return {
        ...state,
        ...initialPhaseState,
        phase: action.data,
      };

    case Code.HAND:
      return {
        ...state,
        hand: action.data,
      };
    case Code.SPREAD:
      return {
        ...state,
        spread: {
          cloak: action.data[Positions.CLOAK],
          center: action.data[Positions.CENTER],
          blind: action.data[Positions.BLIND] ?? null,
        },
      };

    case Code.ATTACK:
      return {
        ...state,
        attacks: [...state.attacks, action.data],
      };

    case Code.END:
      return {
        ...state,
        end: {
          result: action.data,
        },
      };

    case ClientCode.GRABBING:
      return {
        ...state,
        grabbing: !state.grabbing,
      };

    case ClientCode.ATTACKING:
      return {
        ...state,
        attack: action.data,
      };

    case ClientCode.TURN:
      return {
        ...state,
        done: true,
      };

    case ClientCode.READY:
      return {
        ...state,
        ready: true,
      };

    case Code.MALFORMED:
      console.error(action.error);
      return state;

    case Code.IMPOSSIBLE:
      // alert('STOP FUCKING CHEATING')
      console.error(action.error);
      return state;

    default:
      return state;
  }
};

const initialPhaseState = {
  attack: null,

  done: false,

  ready: false,
};

const initialRoundState = {
  hand: [null, null, null, null, null],
  spread: { cloak: null, center: null, blind: null },

  attacks: [],

  grabbing: false,

  ...initialPhaseState,
};

export const initialState = {
  token: null,

  connect: false,

  scores: {
    player: 0,
    opponent: 0,
  },
  round: 0,
  phase: 0,

  ...initialRoundState,

  end: null,
};
