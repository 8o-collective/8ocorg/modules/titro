import React, { createContext, useReducer } from "react";
import { reducer, initialState } from "./reducer.js";

export const GameContext = createContext({
  state: initialState,
  dispatch: () => null,
});

export const GameProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <GameContext.Provider value={[state, dispatch]}>
      {children}
    </GameContext.Provider>
  );
};
