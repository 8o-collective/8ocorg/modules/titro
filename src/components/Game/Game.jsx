// underlying game logic and socket
import React, { useState, useEffect, useContext } from "react";
import { useParams } from "react-router-dom";

import { GameContext } from "contexts/Game/provider.js";

import Table from "./Table.jsx";
import { Code, Result } from "structs/codes.js";

// localStorage.debug = '*'
// localStorage.clear()

const protocol = location.protocol === "https:" ? "wss:" : "ws:";

const Game = () => {
  let { id } = useParams();
  const [socket, setSocket] = useState(null);
  // const [ socket, setSocket ] = useState(new WebSocket(`ws://${HOST}/ws/${id}`))
  const [state, dispatch] = useContext(GameContext);

  const useEffectAfterConnection = (fn, inputs) => {
    useEffect(() => {
      if (socket) return fn();
    }, inputs);
  };

  useEffect(() => {
    let socket = new WebSocket(`${protocol}//${window.host}/ws/${id}`);
    socket.onopen = (e) => console.log(e);
    socket.onclose = (e) =>
      e.code === 1000 ? console.log(e) : window.location.replace("/disconnect");
    socket.onerror = (e) => console.error(e);
    window.onbeforeunload = () => socket.close();
    socket.onmessage = (e) => dispatch(JSON.parse(e.data));
    socket.post = (code, data) =>
      socket.send(JSON.stringify({ token: state.token, code, data }));
    setSocket(socket);

    return () => socket.close();
  }, []);

  useEffectAfterConnection(
    () =>
      (socket.post = (code, data) =>
        socket.send(JSON.stringify({ token: state.token, code, data }))),
    [state.token]
  );

  useEffectAfterConnection(() => {
    if (state.done)
      socket.post(Code.ATTACK, {
        card: state.attack.card,
        position: state.attack.position,
      });
  }, [state.done]);

  useEffectAfterConnection(() => {
    if (state.ready) socket.post(Code.ROUND);
  }, [state.ready]);

  useEffectAfterConnection(() => {
    socket.close(state.end.result === Result.DISCONNECT ? 1006 : 1000);
  }, [state.end]);

  return <Table />;
};

export default Game;
