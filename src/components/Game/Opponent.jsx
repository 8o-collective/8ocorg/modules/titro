// the opponent's Opponent, and notifications of when they've selected their attack
import React, { useState, useEffect, useContext } from "react";

import Card from "./Card.jsx";

import { GameContext } from "contexts/Game/provider.js";

import constants from "assets/constants.js";

import { Rows } from "structs/rows.js";
import { midColumn, positionToColumn } from "structs/positions.js";

let handRemovals = [
  Math.floor(Math.random() * constants.handSize),
  Math.floor(Math.random() * constants.handSize),
];
if (handRemovals[0] === handRemovals[1])
  handRemovals[0] = (handRemovals[1] + 1) % constants.handSize;

const Opponent = () => {
  const [state] = useContext(GameContext);
  const [opponentHand, setOpponentHand] = useState([]);

  // opponent attack positions are negative to flip to the right place in gamespace

  useEffect(() => {
    const opponentAttack = state.attacks.map((e) => e.opponent);

    setOpponentHand(
      Array.from(Array(constants.handSize), (_, i) => {
        if (handRemovals.slice(0, Math.max(state.phase - 1, 0)).includes(i)) {
          return (
            <Card
              key={i}
              coords={[
                positionToColumn(
                  -opponentAttack[handRemovals.indexOf(i)].position
                ),
                Rows.Opponent.ATTACK,
              ]}
              data={opponentAttack[handRemovals.indexOf(i)].card}
            />
          );
        }
        return (
          <Card
            key={i}
            coords={[
              midColumn + i - Math.floor(constants.handSize / 2),
              Rows.Opponent.HAND,
            ]}
            flipped
          />
        );
      })
    );
  }, [state.phase]);

  return opponentHand;
};

export default Opponent;
