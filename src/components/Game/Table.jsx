// underlying CSS grid
import React from "react";

import {
  TableContainer,
  TableParticleContainer,
} from "assets/styles/Game/Table.styles.jsx";
import { ParticleBackground } from "assets/styles/App.styles.jsx";

import Hand from "./Hand.jsx";
import Opponent from "./Opponent.jsx";
import Deck from "./Deck.jsx";
import Spread from "./Spread.jsx";
import Guide from "./Guide.jsx";
import TurnArrow from "./TurnArrow.jsx";
import Score from "./Score.jsx";
import Notification from "./Notification.jsx";
import RoundCount from "./RoundCounter.jsx";

const Table = () => {
  return (
    <TableContainer>
      <TableParticleContainer>{ParticleBackground}</TableParticleContainer>
      <Hand />
      <Opponent />
      <Deck />
      <Spread />
      <TurnArrow />
      <Guide />
      <Score />
      <Notification />
      <RoundCount />
    </TableContainer>
  );
};

export default Table;
