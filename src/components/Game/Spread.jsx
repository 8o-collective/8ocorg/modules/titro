import React, { useState, useEffect, useContext } from "react";

import Card from "./Card.jsx";

import { GameContext } from "contexts/Game/provider.js";

import { Rows } from "structs/rows.js";
import { Positions, positionToColumn } from "structs/positions.js";

const Spread = () => {
  const row = Rows.MID;
  const [state] = useContext(GameContext);
  const [spread, setSpread] = useState(state.spread);

  useEffect(() => setSpread(state.spread), [state.spread]);

  return (
    <>
      <Card
        coords={[positionToColumn(Positions.BLIND), row]}
        data={spread.blind}
        spread={Positions.BLIND}
        flipped
      />
      <Card
        coords={[positionToColumn(Positions.CENTER), row]}
        data={spread.center}
        spread={Positions.CENTER}
      />
      <Card
        coords={[positionToColumn(Positions.CLOAK), row]}
        data={spread.cloak}
        spread={Positions.CLOAK}
      />
    </>
  );
};

export default Spread;
