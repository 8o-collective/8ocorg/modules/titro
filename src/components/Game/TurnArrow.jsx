import React, { useContext } from "react";

import {
  TurnArrowImage,
  TurnArrowTextOverlay,
} from "assets/styles/Game/TurnArrow.styles.jsx";

import { GameContext } from "contexts/Game/provider.js";

import arrow from "assets/images/turn.png";

import { getNameFromValue } from "structs";
import { Phase } from "structs/phase.js";
import { ClientCode, Result } from "structs/codes.js";

const TurnArrow = () => {
  const [state, dispatch] = useContext(GameContext);

  if (
    (state.attack && !state.done) ||
    (state.phase === Phase.CLEANUP && !state.ready)
  ) {
    return (
      <>
        <TurnArrowImage
          src={arrow}
          onClick={() => {
            if (state.end) {
              window.location.replace(
                `/${getNameFromValue(Result, state.end.result)}`
              );
            } else {
              dispatch({
                code:
                  state.phase === Phase.CLEANUP
                    ? ClientCode.READY
                    : ClientCode.TURN,
              });
            }
          }}
        />
        <TurnArrowTextOverlay>next turn</TurnArrowTextOverlay>
      </>
    );
  } else {
    return null;
  }
};

export default TurnArrow;
