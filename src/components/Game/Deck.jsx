// click to see cards remaining, should do animations to get into hand

import React from "react";

import Card from "./Card.jsx";

import constants from "assets/constants.js";

import { Rows } from "structs/rows.js";

const Deck = () => {
  const row = Rows.MID;
  const side = constants.tableColumns;

  return (
    <>
      <Card coords={[side - 1, row]} />
      <Card coords={[2, row]} />
    </>
  );
};

export default Deck;
