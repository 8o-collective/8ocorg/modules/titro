import React, { useContext } from "react";

import {
  PlayerScore,
  OpponentScore,
} from "assets/styles/Game/Score.styles.jsx";

import { GameContext } from "contexts/Game/provider.js";

const Score = () => {
  const [state] = useContext(GameContext);

  return (
    <>
      <PlayerScore>{state.scores.player}</PlayerScore>
      <OpponentScore>{state.scores.opponent}</OpponentScore>
    </>
  );
};

export default Score;
