import React, { useContext } from "react";

import { RoundCounter } from "assets/styles/Game/RoundCounter.styles.jsx";

import constants from "assets/constants.js";
import { GameContext } from "contexts/Game/provider.js";

const RoundCount = () => {
  const [state] = useContext(GameContext);

  return (
    <>
      <RoundCounter>
        {state.round + 1 + "/" + constants.roundLimit}
      </RoundCounter>
    </>
  );
};

export default RoundCount;
