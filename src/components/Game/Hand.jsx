// player's hand, which they can hover over
import React, { useState, useEffect, useContext } from "react";

import Card from "./Card.jsx";

import { GameContext } from "contexts/Game/provider.js";

import { Rows } from "structs/rows.js";
import { midColumn } from "structs/positions.js";

const Hand = () => {
  const row = Rows.Player.HAND;
  const [state] = useContext(GameContext);
  const [hand, setHand] = useState(state.hand);

  useEffect(() => setHand(state.hand), [state.hand]);

  return Array.from({ length: 5 }, (_, i) => (
    <Card
      key={i}
      coords={[midColumn + (i - 2), row]}
      data={hand[i]}
      hand={true}
    />
  ));
};

export default Hand;
