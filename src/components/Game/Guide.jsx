import React, { useContext } from "react";

import Card from "./Card.jsx";

import { GameContext } from "contexts/Game/provider.js";

import { Rows } from "structs/rows.js";
import { midColumn } from "structs/positions.js";

const Guide = () => {
  const row = Rows.Player.ATTACK;
  const [state] = useContext(GameContext);

  if (state.grabbing) {
    return (
      <>
        <Card coords={[midColumn - 3, row]} guide />
        <Card coords={[midColumn, row]} guide />
        <Card coords={[midColumn + 3, row]} guide />
      </>
    );
  } else {
    return null;
  }
};

export default Guide;
