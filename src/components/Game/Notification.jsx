import React, { useState, useEffect, useContext } from "react";

import { NotificationContainer } from "assets/styles/Game/Notification.styles.jsx";

import constants from "assets/constants.js";

import { GameContext } from "contexts/Game/provider.js";
import { Phase } from "structs/phase.js";

const Notification = () => {
  const [notificationText, setNotificationText] = useState(null);
  const [state] = useContext(GameContext);

  useEffect(() => {
    const readableRound = state.round + 2;

    if (!state.connect) setNotificationText("Waiting for player to connect...");
    else if (state.done)
      setNotificationText("Waiting for opponent to finish turn...");
    else if (state.ready && state.phase === Phase.CLEANUP)
      setNotificationText(
        readableRound !== constants.roundLimit
          ? `Waiting for opponent to ready up for round ${state.round + 2} of ${
              constants.roundLimit
            }...`
          : "Waiting for opponent to ready up for final round..."
      );
    else setNotificationText(null);
  }, [state]);

  return (
    notificationText !== null && (
      <NotificationContainer>{notificationText}</NotificationContainer>
    )
  );
};

export default Notification;
