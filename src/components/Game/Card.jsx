import React, { useState, useEffect, useContext, useRef } from "react";

import {
  CardContainer,
  CardVisibleOverlay,
  CardTextOverlay,
} from "assets/styles/Game/Card.styles.jsx";

import { GameContext } from "contexts/Game/provider.js";

import back from "assets/images/back.jpg";
import incorporeal from "assets/images/incorporeal.png";

import constants from "assets/constants.js";

import { getNameFromValue } from "structs";
import { MajorArcana, Rank, Suit } from "structs/cards.js";
import { Phase } from "structs/phase.js";
import { amountRows, Rows } from "structs/rows.js";
import { Positions, columnToPosition } from "structs/positions.js";
import { ClientCode } from "structs/codes.js";

const Card = ({ coords, data, flipped, guide, hand, spread }) => {
  const cardRef = useRef({
    bounds: null,
    attacking: false,
    grabCoords: [null, null],
  });
  const [state, dispatch] = useContext(GameContext);
  const [image, setImage] = useState(guide ? incorporeal : back);
  const [grabbable, setGrabbable] = useState(hand && state.connect);
  const [overlay, setOverlay] = useState("");
  const [mouseOver, setMouseOver] = useState(false);

  const style = {
    transform: flipped ? "scaleY(-1)" : "",
    gridColumn: coords[0],
    gridRow: coords[1],
    cursor: grabbable && !state.done ? "pointer" : "default",
  };

  const setMouseEnter = (b) => setMouseOver(grabbable && b);

  const getCardBounds = () => ({
    left: cardRef.current.getBoundingClientRect().left,
    top: cardRef.current.getBoundingClientRect().top,
  });

  useEffect(
    () => setGrabbable(hand && state.connect && !state.done),
    [state.done]
  );

  useEffect(() => {
    window.addEventListener("resize", handleResize);

    cardRef.current.bounds = getCardBounds();

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  useEffect(() => {
    if (state.phase === Phase.PRIMA) {
      // return all cards to positions and re-render all images
      cardRef.current.style.gridColumn = coords[0];
      cardRef.current.style.gridRow = coords[1];
      cardRef.current.attacking = false;
      cardRef.current.bounds = getCardBounds();
      setGrabbable(hand);
      setImage(guide ? incorporeal : back);
      if (spread === Positions.BLIND)
        cardRef.current.style.transform = style.transform;
      if (spread === Positions.CLOAK)
        setOverlay(<CardVisibleOverlay cloak={true} fade={true} src={back} />);
    }

    // make attack cards from other phases non-grabbable
    if (cardRef.current.style.gridRow.includes(Rows.Player.ATTACK))
      setGrabbable(false);

    if (state.phase === Phase.RESPAIDA) {
      if (spread === Positions.BLIND) cardRef.current.style.transform = "";
      if (spread === Positions.CLOAK) setOverlay(null);
    }

    if (state.phase === Phase.CLEANUP) {
      setGrabbable(false);
      setOverlay(null);
    }
  }, [state.phase]);

  useEffect(() => {
    if (data) {
      setImage(
        require(`../../assets/images/${getNameFromValue(
          Suit,
          data.suit
        )}/${getNameFromValue(
          data.suit === Suit.MAJORARCANA ? MajorArcana : Rank,
          data.rank
        )}.jpg`).default
      );
    } else if (state.phase !== Phase.CLEANUP) {
      setImage(guide ? incorporeal : back);
    }
  }, [data, state.phase]); // state.phase here causes slight flicker effect but stops bug where card faces don't render

  useEffect(() => {
    window.addEventListener("mousedown", handleMouseDown);

    return () => window.removeEventListener("mousedown", handleMouseDown);
  }, [mouseOver]);

  const validMove = (row, column) =>
    row === Rows.Player.ATTACK &&
    columnToPosition(column) !== null &&
    state.attacks.every(
      (attack) => attack.player.position != columnToPosition(column)
    );

  const handleResize = () => (cardRef.current.bounds = getCardBounds());

  const handleMouseDown = (e) => {
    if (e.button !== 0 || !mouseOver) return;

    dispatch({ code: ClientCode.GRABBING }); // toggle grab
    cardRef.current.bounds = getCardBounds();
    cardRef.current.grabCoords = { x: e.pageX, y: e.pageY };

    window.addEventListener("mouseup", handleMouseUp);
    window.addEventListener("mousemove", handleMouseMove);
    e.preventDefault();
  };

  const handleMouseUp = (e) => {
    dispatch({ code: ClientCode.GRABBING }); // toggle grab off

    const row = Math.ceil(e.pageY / (window.innerHeight / amountRows));
    const column = Math.ceil(
      e.pageX / (window.innerWidth / constants.tableColumns)
    );

    if (cardRef.current.attacking && !validMove(row, column)) {
      // moved attack card back to hand
      cardRef.current.style.gridColumn = coords[0];
      cardRef.current.style.gridRow = Rows.Player.HAND;
      dispatch({ code: ClientCode.ATTACKING, data: null });
      cardRef.current.attacking = false;
    } else if (validMove(row, column)) {
      // moved any card to valid attack position
      cardRef.current.style.gridColumn = column;
      cardRef.current.style.gridRow = row;

      if (state.attack && !cardRef.current.attacking) {
        // return last attack card to hand
        state.attack.cardRef.current.style.gridColumn =
          state.attack.originColumn;
        state.attack.cardRef.current.style.gridRow = Rows.Player.HAND;
      }

      dispatch({
        code: ClientCode.ATTACKING,
        data: {
          card: data,
          position: columnToPosition(column),
          cardRef,
          originColumn: coords[0],
        },
      });
      cardRef.current.attacking = true;
    }

    cardRef.current.style.transform = "";
    cardRef.current.bounds = getCardBounds();

    window.removeEventListener("mouseup", handleMouseUp);
    window.removeEventListener("mousemove", handleMouseMove);
    e.preventDefault();
  };

  const handleMouseMove = (e) => {
    let xCoord = e.pageX - cardRef.current.grabCoords.x;
    let yCoord = e.pageY - cardRef.current.grabCoords.y;

    const cardHeight = cardRef.current.clientHeight;
    const cardWidth = cardRef.current.clientWidth;

    // make sure card isn't out of bounds (which would set a scroll and fuck everything)
    if (xCoord + cardWidth > window.innerWidth - cardRef.current.bounds.left) {
      xCoord = window.innerWidth - cardRef.current.bounds.left - cardWidth;
    }
    if (yCoord + cardHeight > window.innerHeight - cardRef.current.bounds.top) {
      yCoord = window.innerHeight - cardRef.current.bounds.top - cardHeight;
    }

    // console.log(`translate(${xCoord}px, ${yCoord}px)`);

    cardRef.current.style.transform = `translate(${xCoord}px, ${yCoord}px)`;
    e.preventDefault();
  };

  return (
    <>
      <CardContainer
        style={style}
        img={image}
        guide={guide}
        ref={cardRef}
        onMouseEnter={() => setMouseEnter(true)}
        onMouseLeave={() => setMouseEnter(false)}
      >
        {overlay}
      </CardContainer>
      {/* {data ? <CardTextOverlay>{data.suit !== Suit.MAJORARCANA ? `${getNameFromValue(Rank, data.rank)} of ${getNameFromValue(Suit, data.suit)}` : `the ${getNameFromValue(MajorArcana, data.rank)}` }</CardTextOverlay> : ''} */}
      {data && (
        <CardTextOverlay>
          {data.suit !== Suit.MAJORARCANA
            ? `${getNameFromValue(Rank, data.rank)} of ${
                getNameFromValue(Suit, data.suit) + "(" + data.rank + ")"
              }`
            : `${
                getNameFromValue(MajorArcana, data.rank) + "(" + data.rank + ")"
              }`}
        </CardTextOverlay>
      )}
    </>
  );
};

export default Card;
