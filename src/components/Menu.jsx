import React from "react";

import {
  MenuContainer,
  MenuTitle,
  MenuParticleContainer,
  MenuButtonContainer,
  MenuButton,
} from "assets/styles/Menu.styles.jsx";

import { ParticleBackground } from "assets/styles/App.styles.jsx";

const Menu = () => {
  return (
    <MenuContainer>
      <MenuParticleContainer>{ParticleBackground}</MenuParticleContainer>
      <MenuTitle>TITRO</MenuTitle>
      <MenuButtonContainer>
        <MenuButton onClick={() => (window.location.href += "game/")}>
          Play
        </MenuButton>
        <MenuButton onClick={() => (window.location.href += "rules/")}>
          Rules
        </MenuButton>
        {/* <MenuButton>Play against a friend</MenuButton> */}
        {/* <MenuButton>Find a lobby</MenuButton> */}
        {/* <MenuButton>Play against a bot</MenuButton> */}
      </MenuButtonContainer>
    </MenuContainer>
  );
};

export default Menu;
