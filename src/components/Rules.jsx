import React from "react";

import card from "assets/images/back.jpg";

import {
  MenuContainer,
  MenuParticleContainer,
  MenuText,
  MenuTextContainer,
  MenuImage,
} from "assets/styles/Menu.styles.jsx";

import { ParticleBackground } from "assets/styles/App.styles.jsx";

const Rules = () => {
  return (
    <MenuContainer>
      <MenuParticleContainer>{ParticleBackground}</MenuParticleContainer>
      <MenuTextContainer>
        <MenuText>
          titro is a trick-taking tarot card game of 6 rounds each consisting of
          5 phases. after 6 rounds, or if either player exceeds 66 points, the
          player with more points wins. ties are not broken.
        </MenuText>
        <MenuText>
          the first phase is <b>prep</b>. each player draws a hand of 5 cards,
          and then 3 cards are placed face down in a row between the players and
          the middle card is flipped face up. these three cards are called the{" "}
          <b>spread</b>. each player may look at the card to their right of the
          face-up center card, but not at the card to their left. the cards from
          left to right respectively are called the <b>blind</b>, the{" "}
          <b>center</b>, and the <b>cloak</b>. your blind is your opponent’s
          cloak, and vice versa.
        </MenuText>
        <MenuText>
          the second phase is called <b>prima</b>, the first half of the trick.
          each player must select a card from their hand and a card in the
          spread to <b>attack</b>. when both players have selected their cards,
          they place them simultaneously face-up below the card that they are
          attacking.
        </MenuText>
        <MenuText>
          the third phase is called <b>respaida</b>. the blind and cloak are
          flipped face up at the beginning of this phase, and then play proceeds
          identically to prima. you may not attack a card that you have already
          attacked.
        </MenuText>
        <MenuText>
          the fourth phase is <b>scoring</b>. when a card from the spread is
          won, the player who won it receives points equal to the absolute
          difference between the values of that card and the card they attacked
          it with. for example, if the center card is a 2 of cups and you attack
          it with a 6 of cups and successfully take the card, you receive 4
          points.
        </MenuText>
        <MenuText>
          if a card is <b>uncontested</b> (attacked by only one player), it is
          won by the attacking player.
        </MenuText>
        <MenuText>
          if a card is <b>contested</b> (attacked by both players), there are
          two criteria for deciding the winner. first, if the suit of the
          attacking card matches the suit of the contested card, it trumps any
          card played out of suit. if a trump doesn’t decide the victor, the
          player who attacked with the higher value card wins. in cases of a
          tie, the card is not won by either player, and neither player receives
          points for it.
        </MenuText>
        <MenuText>
          there is one exception to these rules. <b>THE FOOL</b> cannot win a
          card, even when uncontested. THE FOOL loses against any other attacker
          and also to no attacker, and is essentially the same as not playing a
          card at all.
        </MenuText>
        <MenuText>
          lastly, if either player receives zero points during a round, instead
          they receive the same number of points that the other player received.
          this is called a <b>split</b>. this can either be because the player
          did not win any cards or because they attacked a card with a card of
          the same value, guaranteeing either a loss or a zero point gain.
        </MenuText>
        <MenuText>
          the final phase is <b>clean-up</b>. all cards in play (spread, hands,
          and attackers) go to the discard. discarded cards do not return to
          play for the rest of the game.
        </MenuText>
        <MenuText>
          then, the cycle repeats until the game is over, either by point total
          or round count.
        </MenuText>
        <MenuText>
          thank you for playing titro! we hope you enjoy the game :)
        </MenuText>
        <MenuImage src={card} onClick={() => (window.location.href = "/")} />
      </MenuTextContainer>
    </MenuContainer>
  );
};

export default Rules;
