import React from "react";

import {
  MenuContainer,
  MenuInformation,
  MenuParticleContainer,
  MenuLink,
} from "assets/styles/Menu.styles.jsx";

import { ParticleBackground } from "assets/styles/App.styles.jsx";

import words from "random-words";

const Lobby = () => {
  const id = words({ exactly: 3, maxLength: 10, join: "" });

  return (
    <MenuContainer>
      <MenuParticleContainer>{ParticleBackground}</MenuParticleContainer>
      <MenuInformation>
        Please send your opponent this link, then follow it yourself:
      </MenuInformation>
      <MenuLink href={`game/${id}`}>{`${window.location.href}${id}`}</MenuLink>
    </MenuContainer>
  );
};

export default Lobby;
