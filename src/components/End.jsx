import React from "react";

import {
  EndContainer,
  EndParticleContainer,
  EndText,
} from "assets/styles/End.styles.jsx";

import { ParticleBackground } from "assets/styles/App.styles.jsx";

const Win = () => (
  <EndContainer>
    <EndParticleContainer>{ParticleBackground}</EndParticleContainer>
    <EndText>{`WIN`}</EndText>
  </EndContainer>
);

const Loss = () => (
  <EndContainer>
    <EndParticleContainer>{ParticleBackground}</EndParticleContainer>
    <EndText>{`LOSS`}</EndText>
  </EndContainer>
);

const Tie = () => (
  <EndContainer>
    <EndParticleContainer>{ParticleBackground}</EndParticleContainer>
    <EndText>{`...TIE?`}</EndText>
  </EndContainer>
);

const Disconnect = () => (
  <EndContainer>
    <EndParticleContainer>{ParticleBackground}</EndParticleContainer>
    <EndText>{`The other player has disconnected from the game :(`}</EndText>
  </EndContainer>
);

export { Win, Loss, Tie, Disconnect };
