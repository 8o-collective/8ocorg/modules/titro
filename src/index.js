import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter as Router } from "react-router-dom";

import { routes } from "routes.js";
import { GameProvider } from "contexts/Game/provider.js";

// render the main component
ReactDOM.createRoot(document.getElementById("root")).render(
  <GameProvider>
    <Router>{routes}</Router>
  </GameProvider>
);
