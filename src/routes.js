import React from "react";
import { Routes, Route } from "react-router-dom";

import Menu from "components/Menu.jsx";
import Lobby from "components/Lobby.jsx";
import Game from "components/Game/Game.jsx";
import Rules from "components/Rules.jsx";
import { Win, Loss, Tie, Disconnect } from "components/End.jsx";

const routes = (
  <Routes>
    <Route exact path="/" element={<Menu />} />
    <Route exact path="/game" element={<Lobby />} />
    <Route path="/game/:id" element={<Game />} />
    <Route path="/win" element={<Win />} />
    <Route path="/loss" element={<Loss />} />
    <Route path="/tie" element={<Tie />} />
    <Route path="/disconnect" element={<Disconnect />} />
    <Route path="/rules" element={<Rules />} />
  </Routes>
);

export { routes };
