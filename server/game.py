import asyncio
import titro

from .codes import Code, Result


class Game(titro.Titro):
    """
    Extension of titro.Titro to add sending data over the player connections.
    """
    async def inform(self, code, data):
        """
        A version of broadcast that replaces player data with 'player' keys and 'opponent' keys.
        """
        for player in self.players:
            specdata = {'player' if player.id ==
                        key else 'opponent': value for key, value in data.items()}
            await player.connection.send(code.data(specdata))

    async def prep(self):
        """
        An extension of prep that broadcastes phase data.
        """
        super().prep()
        await self.broadcast(Code.PHASE.data(self.phase.value))
        await self.broadcast(Code.ROUND.data(self.round))
        for player in self.players:
            player.game = self
            player.ready = False
            await player.connection.send(Code.HAND.data([card.asdict() for card in player.hand]))
            await player.connection.send(Code.SPREAD.data(
                {position: card.asdict()
                 for position, card in player.spread().items()}
            ))
        await self.increment_phase()

    async def process(self, request, socket):
        """
        Process each message from the socket.
        """
        if not request.get('token', None):
            # i don't love passing socket, wish i could add as attribute to request
            return await socket.send(Code.MALFORMED.error('No token in request.'))

        player = next(
            (player for player in self.players if player.connection.token ==
             request['token']),
            None)

        if not player:
            return await socket.send(Code.MALFORMED.error('Invalid request token.'))

        match request['code']:
            case Code.ATTACK:
                # try:
                data = request['data']

                player.attack(
                    titro.Card(**data['card']),
                    titro.constants.Positions.Relative(data['position'])
                )

                if all(player.attacking for player in self.players):
                    await self.increment_phase()

                    for player in self.players:
                        player.attacking = None
                # except KeyError:
                # 	await player.connection.send(Code.MALFORMED.error('Invalid data values.'))
            case Code.ROUND:
                if self.phase != titro.constants.Phase.CLEANUP:
                    return await player.connection.send(
                        Code.IMPOSSIBLE.error('Cannot ready before cleanup.')
                    )

                player.ready = True

                if all(player.ready for player in self.players):
                    await self.prep()
            case _:
                await player.connection.send(Code.MALFORMED.error('Invalid request code.'))

    async def increment_phase(self):
        """
        Increments phase and broadcasts new phase code.
        """
        if self.phase in (titro.constants.Phase.PRIMA, titro.constants.Phase.RESPAIDA):
            await self.inform(Code.ATTACK, {player.id: player.attacking for player in self.players})
        for player in self.players:
            await player.connection.send(Code.SPREAD.data(
                {position: card.asdict()
                 for position, card in player.spread().items()}
            ))

        # super().increment_phase()
        self.phase = self.phase.next()
        await self.broadcast(Code.PHASE.data(self.phase.value))

        if self.phase is titro.constants.Phase.CLEANUP:
            await self.cleanup()

    async def cleanup(self):
        """
        An extension of cleanup that broadcastes phase data.
        """
        for player in self.players:
            await player.connection.send(Code.SPREAD.data(
                {position: card.asdict()
                 for position, card in player.spread().items()}
            ))

        super().cleanup()

        await self.inform(Code.SCORE, {player.id: player.score for player in self.players})

    async def end_signal(self):
        """
        Sends ending codes to the users after the game over condition is met.
        """
        if self.winner is None:
            await self.broadcast(Code.END.data(Result.TIE.value))
        else:
            await self.winner.connection.send(Code.END.data(Result.WIN.value))
            await self.loser.connection.send(Code.END.data(Result.LOSS.value))

    def end_game(self):
        """
        A wrapper of end_signal that schedules it on the event loop.
        """
        super().end_game()
        asyncio.create_task(self.end_signal())
