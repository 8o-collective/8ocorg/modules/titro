from .connection import Connection
from .codes import Code
from .game import Game


class Room:
    """
    Represents a single game and loads connections for each player.
    """

    def __init__(self, close):
        self.connections = [None, None]  # player 1 and 2
        self.game = Game()
        self.close = close

    async def broadcast(self, message):
        """
        Sends a message to every connection in the room.
        """
        for connection in self.connections:
            if connection:
                await connection.send(message)

    async def connect(self, websocket):
        """
        Adds a new connection to the room and broadcastes a disconnect when it closes.
        """
        await websocket.accept()

        connection = await Connection.create(websocket)

        # replace first None with connection
        self.connections[self.connections.index(None)] = connection

        # if all players are connected, add connections to player objects and start game
        if all(self.connections):
            await self.broadcast(Code.CONNECT)  # all players in lobby
            for player, connection in zip(self.game.players, self.connections):
                player.connection = connection
            self.game.broadcast = self.broadcast  # game object takes over from here
            await self.game.prep()

    async def disconnect(self, websocket):
        """
        Manages disconnection from a socket currently in a room.
        """
        connection = next(
            connection for connection in self.connections if connection.websocket == websocket)
        self.connections.remove(connection)
        if any(self.connections):
            await self.broadcast(Code.DISCONNECT)
        else:
            self.close()  # delete room if no connections left


class Manager:
    """
    Manages all open rooms.
    """
    rooms = {}

    def create(self, id):
        """
        Creates a room and attaches a method to close it if all connections close.
        """
        room = Room(close=lambda: self.rooms.pop(id, None))
        self.rooms[id] = room
        return room

    def join(self, id):
        """
        Closes the room to new connections if both players are in
        """
        # id is just for connection, we remove it once both players are in
        return self.rooms.pop(id, None)
