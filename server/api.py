from fastapi import WebSocket, WebSocketDisconnect

from .manager import Manager

from . import app

manager = Manager()


@app.websocket("/ws/{id}")
async def ws(websocket: WebSocket, id: str):
    """
    Entrypoint for the websocket connection.
    """
    room = manager.join(id) or manager.create(id)

    await room.connect(websocket)

    try:
        while True:
            request = await websocket.receive_json()
            await room.game.process(request, websocket)
    except WebSocketDisconnect:
        await room.disconnect(websocket)
