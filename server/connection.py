from secrets import token_hex

from .codes import Code


class Connection():
    """
    An async queue representing each message sent over the websocket.
    Anything put on the queue will be sent.
    """

    def __init__(self, websocket):
        super().__init__()
        self.token = token_hex(16)
        self.websocket = websocket

    @classmethod
    async def create(cls, websocket):
        """
        Creates connection and sends a token over itself.
        """
        self = Connection(websocket)
        await self.send(Code.TOKEN.data(self.token))
        return self

    def send(self, message):
        """
        Sends a dict message over the socket.
        """
        if isinstance(message, Code):
            message = message.asdict()
        return self.websocket.send_json(message)
