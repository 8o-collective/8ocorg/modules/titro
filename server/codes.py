from enum import Enum


class Code(int, Enum):
    """
    Enum representing all datacodes sent between the server and client.
    """
    TOKEN = 0		# token to use in requests to server

    CONNECT = 1		# another player has connected
    DISCONNECT = 2  # other player disconnected

    SCORE = 3		# update score
    ROUND = 4		# round change, or client is ready to continue to next round
    PHASE = 5		# phase change

    HAND = 6		# array of player hand
    SPREAD = 7		# array of table spread

    ATTACK = 8		# position and value of attacking card for round from client

    END = 9		    # game end signal

    MALFORMED = -1  # input is malformed or cannot be parsed
    IMPOSSIBLE = -2  # move chosen is desynchronized from server game state

    def asdict(self):
        """
        Returns Code enum as a dict so that it can be merged with a data dict.
        """
        return {'code': self.value}

    def data(self, data=None):
        """
        Returns a merged Code dict and data dict.
        """
        return self.asdict() | ({'data': data} if data is not None else {})

    def error(self, error=None):
        """
        Returns a merged Code dict and error dict.
        """
        return self.asdict() | ({'error': error} if error is not None else {})


class Result(int, Enum):
    """
    Enum representing 3 possible non-disconnect endings.
    Disconnect endings are handled by the client.
    """
    TIE = -1
    LOSS = 0
    WIN = 1
